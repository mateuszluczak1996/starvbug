

class Obstacle extends GameObject {


    constructor(image, width, height, x) {
        super();
        this.image = image;
        this.width = width;
        this.height = height;
        this.randomStartDirection = Math.round(Math.random());
        this.x = x;
        this.y = -height;
        this.collision = false;
        this.horizontalSpeed = 3;
        this.setHorizontalSpeed(LEVEL);

    }


    setCollision(collision_) {
        hitObstacle.play();
        this.collision = collision_;
    }

    getCollision() {
        return this.collision;
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    updateState() {

        this.y += 2;

        if (LEVEL == 2 || LEVEL == 3) {

            if (this.randomStartDirection == 0) {
                if (this.x > 0) {
                    this.x -= this.horizontalSpeed;
                 
                } else {
                    this.randomStartDirection = 1;
                }
            } else {
                if (this.x + this.width < canvas.width) {
                    this.x += this.horizontalSpeed;
                } else {
                    this.randomStartDirection = 0;
                }
            }
        }




    }

    render() {
        ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
       

    }


    setHorizontalSpeed(LEVEL){
        if(LEVEL==2){
            this.horizontalSpeed=3;
        } else if(LEVEL==3){
            this.horizontalSpeed=6;
        }
    }



    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

}