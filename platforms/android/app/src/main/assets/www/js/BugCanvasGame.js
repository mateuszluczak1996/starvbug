

class BugCanvasGame extends CanvasGame {
    constructor() {
        super();

        this.startGame();
        this.screenShakeInterval = null;
        this.screenIsRotatingToTheLeft = false;
        this.NUMBER_OF_SCREEN_SHAKES_INTERATIONS = 10;
        this.numberOfScreenShakes = 0;
        this.renderObjects = true;
        this.requestId = undefined;

    }



    collisionDetection() {


        if (!ctxCollision) {
            return;
        }


        for (let index = 0; index < obstacles.length; index++) {


            if (!obstacles[index].getCollision() && (obstacles[index].getY() > 0 && obstacles[index].getY() < canvas.height)) {



                let $boundingBoxColison = this.isCollide(obstacles[index], bug);

                if ($boundingBoxColison) {

                    let imageData = ctxCollision.getImageData(obstacles[index].getX(), obstacles[index].getY(), obstacles[index].getWidth(), obstacles[index].getHeight());
                    let data = imageData.data;

                    for (var i = 0; i < data.length; i += 4) {
                        if (data[i + 3] !== 0) {
                            obstacles[index].setCollision(true);
                            window.navigator.vibrate(500);
                            life--;
                            break;
                        }
                    }
                }
            }

        }


        for (let index = 0; index < food.length; index++) {

            if (!food[index].getCollision() && (food[index].getY() > 0 && food[index].getY() < canvas.height)) {

                let $boundingBoxColison = this.isCollide(food[index], bug);

                if ($boundingBoxColison) {

                    let imageData = ctxCollision.getImageData(food[index].getX(), food[index].getY(), food[index].getWidth(), food[index].getHeight());
                    let data = imageData.data;

                    for (var i = 0; i < data.length; i += 4) {
                        if (data[i + 3] !== 0) {
                            food[index].setCollision(true);
                            score++;
                            this.setExtraLife(score);
                            break;
                        }
                    }
                }
            }

        }

    }

    setExtraLife(score){
        if(score>=15 && score<16){
            life++;
        } else if(score>=30 &&score<31){
            life++;
        }
    }


    render() {


        if (this.gameRun) {

            super.render();


            document.getElementById("life").innerHTML = "Life:" + life;
            document.getElementById("score").innerHTML = "Score:" + score;

            if (life <= 0) {
                this.stopGame();
                showDialog();
                if (score > 0) {
                    writeData();
                }

            }

            backgroundImage.render();

            for (let i = 0; i < obstacles.length; i++) {
                obstacles[i].render();
            }


            for (let k = 0; k < food.length; k++) {
                if (food[k].isDisplayed())
                    food[k].render();

            }

            bug.render();

            if (text.isDisplayed())
                text.render();

        }

    }



    startGame() {
        this.gameRun = true;
    }

    stopGame() {
        this.gameRun = false;
    
    }


    isCollide(a, b) {
        return !(
            ((a.y + a.height) < (b.y)) ||
            (a.y > (b.y + b.height)) ||
            ((a.x + a.width) < b.x) ||
            (a.x > (b.x + b.width))
        );
    }


}