
window.addEventListener("load", onAllAssetsLoaded);


let state = null;
let user=null;



function onAllAssetsLoaded() {
    state = document.getElementById("state");
    user=document.getElementById("username").value;

}



function gamePage() {
    saveInfo(user);
    window.location = "bug_game.html";
}


function saveInfo(user){
    if(user==null) return;
    localStorage.user=document.getElementById("username").value;
}

function toggleState() {
    if (state.innerHTML === "Play") {
        state.innerHTML = "Pause";
        game.stopGame();
    } else if (state.innerHTML === "Pause") {
        state.innerHTML = "Play";
        game.startGame();
    }
}

function showDialog() {
    var button = document.getElementById("restart");
    if (button.style.display === "none") {
        button.style.display = "block";
        
    }
}

function restartGame() {
    var button = document.getElementById("restart");
    if (button.style.display !== "none") {
        button.style.display = "none";
        window.location.reload();
    }
}
