
var table;
var caption;
var thead;
var rowThead;
var col1Th;
var col2Th;


function writeData() {
    var db = firebase.firestore();

    db.collection("score_collection").add({
        score: score,
        name: localStorage.user
    });
}


function getData() {

    var db = firebase.firestore();
    
    highScoreCollection = db.collection("score_collection").where("score",">",0).orderBy("score","desc").limit(10);

    highScoreCollection.get().then(snapshot => {

        table = document.createElement('table');
        caption=document.createElement('caption');
        caption.innerHTML="High scores";
        table.appendChild(caption);
        thead = document.createElement('thead');
        rowThead = document.createElement("tr");
        col1Th = document.createElement("th");
        col1Th.setAttribute("scope","col");
        col2Th = document.createElement("th");
        col2Th.setAttribute("scope","col");
        col1Th.innerHTML = "Username";
        col2Th.innerHTML = "Score";
        table.appendChild(thead);
        thead.appendChild(rowThead);
        rowThead.appendChild(col1Th);
        rowThead.appendChild(col2Th);
        
        snapshot.forEach(doc => {
            var data = doc.data();
            createTable(data.score, data.name);
        })

        document.body.appendChild(table);
    })


}

function createTable(score, name) {

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.setAttribute("data-label","Username");
    var td2 = document.createElement('td');
    td2.setAttribute("data-label","Score");

    var text1 = document.createTextNode(name);
    var text2 = document.createTextNode(score);

    td1.appendChild(text1);
    td2.appendChild(text2);
    tr.appendChild(td1);
    tr.appendChild(td2);

    table.appendChild(tr);


}