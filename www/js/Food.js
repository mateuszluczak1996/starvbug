class Food extends GameObject  {


    constructor(image, width, height,x) {
        super();
        this.image = image;
        this.width = width;
        this.height = height;
        this.speed=2;
        this.x=x;
        this.startPosition=-height-getRandomInt(50,80);
        this.y = this.startPosition;
       
      
    
    }
    

    setCollision(collision_){
        eatFood.play();
        this.stopAndHide();
        foodCounter++;
        this.collision=collision_;
    }

    getCollision(){
        return this.collision;
    }


    updateState() {

        this.y+=this.speed;
        
    }

    render() {
        ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    
    }

    

    setX(x_){
        this.x=x_;
    }

    setY(y_){
        this.y=y_;
    }
    

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getStartPosition(){
        return this.startPosition;
    }

}