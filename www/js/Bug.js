/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland. */



class Bug extends GameObject {
    /* Each gameObject MUST have a constructor() and a render() method.        */
    /* If the object animates, then it must also have an updateState() method. */

    lastAlphaState = 0.0;
    alphaChange = false;


    constructor(image, width, height, updateStateMilliseconds) {
         super(); /* as this class extends from GameObject, you must always call super() */


     

        /* These variables depend on the object */
        this.image = image;
        this.width = width;
        this.height = height;
        this.x = (canvas.width / 2) - (width / 2);
        this.y = (canvas.height - 1.2*height);
      
    }


   

    updateState() {
    
        
      
        if (this.lastAlphaState != 0 && this.lastAlphaState < -2 && this.lastAlphaState > -90.0 && this.x > 0) {
            let left=this.x - Math.abs(this.lastAlphaState)*0.5;
            
            if(left>0){
                this.x=left;
            
            } else {
                let start=Math.abs(this.lastAlphaState)*0.9;
                for(let i=start; i>0; i--){
                    if(this.x-i>0){
                        this.x=this.x-i;
                        break;
                    }
                }
               
            }
    
        }

        if (this.lastAlphaState != 0 && this.lastAlphaState > 2 && this.lastAlphaState < 90.0 && this.x + this.width < canvasCollision.width) {
           let right = this.x + Math.abs(this.lastAlphaState)*0.5;
            if(right+this.width<canvasCollision.width){
                this.x=right;
            } else {
                let start=this.x + this.width;
                let alpha=Math.abs(this.lastAlphaState)*0.9;
                for(let i=alpha; i>0;i--){
                    if(start+i<canvasCollision.width){
                        this.x=this.x+i;
                        break;
                    }
                }
            }
        }

       

    }

    setX(alpha) {
        this.alphaChange = true;
        if (alpha != this.lastAlphaState) {
            this.lastAlphaState = alpha;
            this.alphaChange = false;
        }
    }

    render() {
        // ctx.drawImage(this.image, this.x, this.y, this.width, this.height);

        ctxCollision.drawImage(this.image, this.x, this.y, this.width, this.height);
    }

    
}