// /* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland.             */
// /* There should always be a javaScript file with the same name as the html file. */
// /* This file always holds the playGame function().                               */
// /* It also holds game specific code, which will be different for each game       */




// /******************** Declare game specific global data and functions *****************/
// /* images must be declared as global, so that they will load before the game starts  */
let game =null;
let scrollingBackgroundImage = new Image();
scrollingBackgroundImage.src = "img/test.png";

var hitObstacle = new Audio('audio/crash.mp3');
var eatFood=new Audio('audio/crunch.mp3');

var foodImages = [
    "img/strawberry.png",
     "img/watermelon.png",
     "img/tomato.png",
     "img/red_apple.png",
     "img/green_apple.png"
  
];

let bugImage = new Image();
bugImage.src = "img/monster.png";
let bug = null;
let obstacles = [];
let nextToEachOther = [];
let food = [];
let backgroundImage = null;



let obsctacleActive = 0;
let foodCounter = 0;
let lastFoodCounter = 0;
let score = 0;
let life = 2;
let text = null;


let obstacleImage = new Image();
obstacleImage.src = "img/obstacle.png";

window.addEventListener('deviceorientation', handlerOrientation)
window.addEventListener('resize', this.resizeCanvas, false);

function handlerOrientation(ev) {
    if (ev.gamma != undefined) {

        bug.setX(ev.gamma);
    }

}


let LEVEL = 1;
let bat = null;
let numberOfObstacles = getRandomNumberOfObstacles();


function getRandomNumberOfObstacles() {
    if (LEVEL == 1) {
        return getRandomInt(10,20);
    } else if (LEVEL == 2) {
        return getRandomInt(10,20);
    } else if(LEVEL ==3) {
       return getRandomInt(100,200);
    }
}


function getRandomFood() {
    var randomFood = Math.floor(Math.random() * foodImages.length);
    let image = new Image();
    image.src = foodImages[randomFood];
    return image;
}



function setObstacles() {
    numberOfObstacles = getRandomNumberOfObstacles();
    let counter = 0;
    if (LEVEL == 1) {

        while (counter < numberOfObstacles) {

            let width = getRandomInt(1 / 3 * canvas.width, 1 / 2 * canvas.width);
            obstacles[counter] = new Obstacle(obstacleImage, width, 1 / 20 * canvas.height, getRandomInt(0, canvas.width - width));
            counter++;



        }




    } else if (LEVEL == 2 || LEVEL==3) {
        while (counter < numberOfObstacles) {
            let width = getRandomInt(1 / 8 * canvas.width, 1 / 4 * canvas.width);
            obstacles[counter] = new Obstacle(obstacleImage, width, 1 / 20 * canvas.height, getRandomInt(0, canvas.width - width));
            counter++;



        }
    }
}


function runObstacleInterval() {
    
    if (obstacles[obsctacleActive] != null) {
        obstacles[obsctacleActive].start();
        $randomFood = getRandomInt(1, 2);


        if ($randomFood == 1) {
            runFoodInterval();

        }

    }


}


function runFoodInterval() {
    if (typeof food[foodCounter] === 'undefined') {
        let x = obstacles[obsctacleActive].getX();
        let y = obstacles[obsctacleActive].getY();
        let width = obstacles[obsctacleActive].getWidth();
        food[foodCounter] = new Food(getRandomFood(), 30, 20, x);
        food[foodCounter].setX(getRandomInt(0, canvas.width - width));
        food[foodCounter].start();
    } else {
        if (food[foodCounter].getY() > canvas.height) {

            foodCounter++;
        }
    }


}


function secondRound() {

    

    const intervalSecondLevel = setInterval(function () {

        if (obsctacleActive >= numberOfObstacles && obstacles[obstacles.length - 1].getY() > canvas.height) {
               obsctacleActive = 0;
                LEVEL++;
                setObstacles();
                clearInterval(intervalSecondLevel);
                text.setText("Prepare to round " + LEVEL);
                text.start();
                thirdRound();
            

        } else {
            text.stopAndHide();
            runObstacleInterval();
            obsctacleActive++;
        }




    }, 1000);
}


function thirdRound(){
   setInterval(function () {

        if (obsctacleActive >= numberOfObstacles && obstacles[obstacles.length - 1].getY() > canvas.height) {
            obstacles=[];
            setObstacles();
            obsctacleActive=0;



        } else {
             text.stopAndHide();
            runObstacleInterval();
            obsctacleActive++;
        }




    }, 800);
}


function playGame() {

    game = new BugCanvasGame();
 

    bug = new Bug(bugImage, 40, 30, 2);
    bug.start();
    
    
    
    text = new StaticText("Prepare to round " + LEVEL, 1 / 2 * canvas.width, canvas.height / 2, "Verdana", 25, "green");
    text.start();
    backgroundImage = new ScrollingBackgroundImage(scrollingBackgroundImage, 20);
    backgroundImage.start();

   

    setObstacles();

    if (LEVEL == 1) {
        
        const interval = setInterval(function () {

            if (obsctacleActive >= numberOfObstacles && obstacles[numberOfObstacles - 1].getY() > canvas.height) {
                
                obsctacleActive = 0;
                LEVEL++;
                setObstacles();
                clearInterval(interval);
                text.setText("Prepare to round " + LEVEL);
                text.start();
                secondRound();

            } else if (obsctacleActive >= numberOfObstacles) {

                //  text.setText("Prepare to round " + LEVEL);
               
            }
            else if (obsctacleActive <= numberOfObstacles) {
                text.stopAndHide();
                runObstacleInterval();
                obsctacleActive++;

            }




        }, 1300);

    }





     
     

     game.start();

}
